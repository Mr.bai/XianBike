package com.baikeyang.bike.controller;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class BaseController {

	Map<String, Object> jsonData = new HashMap<String, Object>();

	protected String parse2Json(Object obj) {
		return JSONObject.toJSONString(obj);
	}

	protected String parse2JsonArr(Object obj) {
		return JSONArray.toJSONString(obj);
	}

	protected String parse2Json(String key, Object data) {
		jsonData.put(key, data);
		return JSONObject.toJSONString(jsonData);
	}

}
