package com.baikeyang.bike.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baikeyang.bike.service.IXABikeService;

/**
 * 同步所有站点位置信息任务(一天一次) 
 * @author Administrator
 *
 */
@Component
public class BikeSitePostionJob {
	
	private Logger logger = Logger.getLogger(BikeSitePostionJob.class);
	
	@Autowired
	private IXABikeService xaBikeService;
//
	public void execute(){
		try {
			logger.info("开始同步[站点位置信息]计划任务开始执行...");
			xaBikeService.synchronizeBikeSitePosition();
			logger.info("开始同步[站点位置信息]计划任务执行完毕...");
		} catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
