package com.baikeyang.bike.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baikeyang.bike.service.IXABikeService;

/**
 * 同步正常站点车辆信息任务
 * @author Administrator
 *
 */
@Component
public class BikeNormalSiteJob {
	
	private Logger logger = Logger.getLogger(BikeNormalSiteJob.class);
	
	@Autowired
	private IXABikeService xaBikeService;
//
	public void execute(){
		try {
			logger.info("开始同步[正常站点信息]计划任务开始执行...");
			xaBikeService.synchronizeBikeNormalSite();
            logger.info("开始同步[正常位置信息]计划任务执行完毕...");
        } catch(Exception e) {
        	logger.error(e.getMessage(), e);
        }
	}
}
