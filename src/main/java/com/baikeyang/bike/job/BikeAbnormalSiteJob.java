package com.baikeyang.bike.job;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.aspectj.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baikeyang.bike.service.IXABikeService;

/**
 * 同步异常站点车辆信息任务(12小时同步一次)
 * @author Administrator
 *
 */
@Component
public class BikeAbnormalSiteJob {
	
	private Logger logger = Logger.getLogger(BikeAbnormalSiteJob.class);
	
	@Autowired
	private IXABikeService xaBikeService;
//
	public void execute(){
		try {
			logger.info("开始同步[异常站点信息]计划任务开始执行...");
			xaBikeService.synchronizeBikeAbnormalSite();
            logger.info("开始同步[异常站点信息]计划任务执行完毕...");
        } catch(Exception e) {
        	logger.error(e.getMessage(), e);
        }
	}
	
	public static void main(String[] args) {
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
			String time = sdf.format(new Date());
			writeTime(time);
			InputStream is = getTimeFileStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String str = br.readLine();
			System.out.println(str);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static boolean writeTimeFile(){
		try{
			File file = new File(getFilePath());
			if(!file.exists()){
				if(!file.getParentFile().exists()){
					file.getParentFile().mkdirs();
				}
				return file.createNewFile();
			}else{
				return true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public static String getFilePath(){
		String path = "d:/disk/job/bike/job.data";
		return path;
	}
	
	/**
	 * 写日期数据到文件中
	 * @param time
	 */
	public static void writeTime(String time) {
		synchronized (time) {
			if(null == time || "".equals(time)) return;
			File timeFile = new File(getFilePath());
			if(!timeFile.exists()) {
				writeTimeFile();
			}
			OutputStream os = null;
			try {
				os = new FileOutputStream(timeFile, false);
				os.write(time.getBytes());
				os.flush();
			} catch (FileNotFoundException e) {
			} catch (IOException e) {
			} finally {
				if(null != os) {
					try {
						os.close();
					} catch (IOException e) {
					}
				}
			}
		}
		
	}

	/**
	 * 获取文件流
	 * @return
	 */
	private static InputStream getTimeFileStream() {
		try {
			InputStream is = null;
			while(true) {
				is = FileUtil.class.getClassLoader().getResourceAsStream("/data/time.data");
				if(is == null) {
					if(writeTimeFile()){
						is = new BufferedInputStream(new FileInputStream(getFilePath()));
						break;
					}
				} else {
					break;
				}
			}
			return is;
		} catch(Exception e) {
			return null;
		}
	}
	
    public static String getTime() {
    	String time = null;
        try {
        	InputStream is = getTimeFileStream();
        	InputStreamReader isr = new InputStreamReader(is);
        	BufferedReader br = new BufferedReader(isr);
        	time = br.readLine();
        } catch (IOException e) {
        }
        return time;
    }
    
}
