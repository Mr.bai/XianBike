package com.baikeyang.bike.job.Exception;

public class BikeException extends RuntimeException {

	private Integer errorCode;

	private String errorMessage;

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = -67207521535748472L;

	public BikeException() {
		super();
	}

	public BikeException(Integer errorCode, String message, Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
		this.errorMessage = message;
	}

	public BikeException(String message, Throwable cause) {
		super(message, cause);
	}

	public BikeException(String message) {
		super(message);
	}

	public BikeException(Throwable cause) {
		super(cause);
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public BikeException(Integer errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
}
