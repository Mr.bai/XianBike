package com.baikeyang.bike.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baikeyang.bike.service.IXABikeService;

@Component
public class BikeJob {

	
	private Logger logger = Logger.getLogger(BikeJob.class);
	
	@Autowired
	private IXABikeService xaBikeService;

	public void execute(){
		try {
			logger.info("开始同步[站点信息]计划任务开始执行...");
			xaBikeService.synchronizeBikeSite();
            logger.info("开始同步[站点信息]计划任务执行完毕...");
        } catch(Exception e) {
        	logger.error(e.getMessage(), e);
        }
	}
	
}
