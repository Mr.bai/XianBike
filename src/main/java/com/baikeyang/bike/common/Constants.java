package com.baikeyang.bike.common;

public class Constants {

	public static class RedisKey {
		public static final String 异常车辆 = "bike-abnormal-position";
		public static final String 异常车辆AMAP = "bike-abnormal-position-amap";
		public static final String 正常车辆 = "bike-normal-position";
		public static final String 正常车辆AMAP = "bike-normal-position-amap";
	}

}
