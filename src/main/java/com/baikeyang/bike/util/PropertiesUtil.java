package com.baikeyang.bike.util;

import java.util.ResourceBundle;

public class PropertiesUtil {
	private static final String CONFIG_FILE_NAME = "config";

	private static ResourceBundle bundle = null;

	public static final String getValue(String key) {
		String val = null;
		if (StringUtil.isEmpty(key))
			return null;

		try {
			if (bundle == null)
				bundle = ResourceBundle.getBundle(CONFIG_FILE_NAME);

			if (bundle != null)
				val = bundle.getString(key);
		} catch (Exception e) {
		}
		return val;
	}
	
	public static final String getValue(String key,String defaultVal) {
		String val = getValue(key);
		if (StringUtil.isEmpty(val)){
			val = defaultVal;
		}
		return val;
	}

	public static void main(String[] args) {
		System.out.println(getValue("CACHE_LIST"));
	}
}
