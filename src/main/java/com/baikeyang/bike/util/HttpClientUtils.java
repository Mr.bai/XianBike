package com.baikeyang.bike.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

public class HttpClientUtils {

	private static Logger log = Logger.getLogger(HttpClientUtils.class);
	
	public static String doPost(String url, Map<String, String>requestHeaderMap, Map<String, String> parameterMap){
		String result = "";
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(url);
		
		// Header(其中必要的只有cookie中的appver。而且如果要调用api，必须加上Referer，只要是music.163.com的就可以)
		post.setRequestHeader(new Header("User-Agent", "Mozilla/5.0 /Windows; U; Windows NT 4.1; de; rv:1.9.1.5) Gecko/20091102 Firefox/3.0;"));
		if(null != requestHeaderMap && !requestHeaderMap.isEmpty()){
			Iterator iter = requestHeaderMap.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry entry = (Map.Entry) iter.next();
				Object key = entry.getKey();
				Object val = entry.getValue();
				post.setRequestHeader(key.toString(), val.toString());
			}
		}
		
		// Parameter
		if(null != parameterMap && !parameterMap.isEmpty()){
			Iterator iter = parameterMap.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry entry = (Map.Entry) iter.next();
				Object key = entry.getKey();
				Object val = entry.getValue();
				post.addParameter(key.toString(), val.toString());
			}
		}
	    try {
			client.executeMethod(post);
			result = post.getResponseBodyAsString();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public static String doGet(String url, Map<String, String> parameterMap){
		String response = "";
		HttpClient client = new HttpClient();
		GetMethod method = new GetMethod(url);
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		NameValuePair[] valuePairs = new NameValuePair[]{};
		// Parameter
		if(!parameterMap.isEmpty()){
			Iterator iter = parameterMap.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry entry = (Map.Entry) iter.next();
				Object key = entry.getKey();
				Object val = entry.getValue();
				NameValuePair pair = new NameValuePair(key.toString(), val.toString());
				pairs.add(pair);
			}
			valuePairs = pairs.toArray(valuePairs);
		}
		method.setQueryString(valuePairs);
		try {
			client.executeMethod(method);
			response = new String(method.getResponseBodyAsString());
			method.releaseConnection();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			method.releaseConnection();
		}
		return response;
	}
	
}
