package com.baikeyang.bike.util;

/**
 * 地图相关API处理接口
 * @author BaiKeyang
 * @since 2014-3-12
 * @version 1.0
 */
public class MapUtil {
	
	/**
	 * 判断点是否在圆形区域内
	 * @param pointLng 点的经度
	 * @param pointLat 点的纬度
	 * @param cenLngX 圆的中心点经度
	 * @param cenLatY 圆的中心点纬度
	 * @param radius 半径，单位：米
	 * @return 
	 */
	public static boolean isPointInCircleArea(double pointLng, double pointLat, double cenLngX, double cenLatY, double radius) {
	    return MapCommonUtil.isPointInCircleArea(pointLng, pointLat, cenLngX, cenLatY, radius);
	}
	
}
