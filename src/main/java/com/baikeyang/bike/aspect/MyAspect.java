package com.baikeyang.bike.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAspect {

	private final static Logger log = Logger.getLogger(MyAspect.class);
	@Before("execution(* com.baikeyang.service..*Impl.*(..))")
	public void cutSayHello(){
		log.info("我要开始切面了");
		//System.out.println("我要开始切面了 cut Say Hello");
	}
	
}