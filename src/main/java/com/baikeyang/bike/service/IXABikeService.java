package com.baikeyang.bike.service;

import java.util.List;

import com.baikeyang.bike.bean.BikeBean;

public interface IXABikeService {
	
	public void sayHello(String str);
	/**
	 * 查询昨日租车数量
	 * @return
	 */
	String findBikeTradeCount();
	
	/**
	 * 查询正常站点信息(该接口信息获取过于缓慢)
	 * @return
	 */
	String findBikeNormalSite();

	/**
	 * 获取所有正常站点信息(该接口直接从Redis中获取数据)
	 * @return
	 */
	List<BikeBean> getBikeNormalSite();
	
	/**
	 * 查询异常站点信息(该接口信息获取过于缓慢)
	 * @return
	 */
	String findBikeAbnormalSite();

	/**
	 * 获取所有异常站点信息(该接口直接从Redis中获取数据)
	 * @return
	 */
	List<BikeBean> getBikeAbnormalSite();
	
	/**
	 * 根据卡号查询租赁历史记录
	 * @param cardNo
	 * @return
	 */
	String findBikeTradeRecords(String cardNo);
	
	/**
	 * 同步站点车辆信息
	 */
	void synchronizeBikeSite();
	void synchronizeBikeNormalSite();
	void synchronizeBikeAbnormalSite();
	
	/**
	 * 同步站点位置信息
	 * 说明：每1天同步一次站点位置信息
	 */
	void synchronizeBikeSitePosition() throws Exception ;

	/**
	 * 获取所有到站点信息和车辆信息(该接口信息获取过于缓慢)
	 * @return
	 * @throws Exception
	 */
	List<BikeBean> getAllBikes() throws Exception ;

	/**
	 * 获取所有车辆站点位置信息
	 * @return
	 * @throws Exception
	 */
	public List<BikeBean> getAllBikePostions(int convertType);
	public BikeBean getBikeSiteById(String siteId);
	

	public List<BikeBean> bikeLocalConvert(List<BikeBean> bikes, List<BikeBean> resules, int convertType);
//	public List<BikeBean> bikeLocalConvert(List<BikeBean> bikes, List<BikeBean> resules, int convertType, Integer count) throws InterruptedException;
	
}
