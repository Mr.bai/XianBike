package com.baikeyang.bike.ws.service.impl;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import com.baikeyang.bike.ws.service.ITestService;

@WebService
@SOAPBinding(style = Style.RPC)
public class TestServiceImpl implements ITestService {

	@Override
	public String queryStr() {
		return "Hello WebService ~";
	}

	@Override
	public String sayHello(String sayStr) {
		return "你要说的话是:"+sayStr;
	}

}
