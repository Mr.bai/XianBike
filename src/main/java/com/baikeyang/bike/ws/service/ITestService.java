package com.baikeyang.bike.ws.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface ITestService {
	
	@WebMethod
	public String queryStr();
	
	@WebMethod
	public String sayHello(@WebParam(name="sayStr",partName="sayStr")String sayStr);

}
