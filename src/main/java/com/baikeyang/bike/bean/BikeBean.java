package com.baikeyang.bike.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BikeBean {
	
	private String emptynum;
	
	private String latitude;
	
	private String longitude;
	
	private String lnglat;
	
	private String location;
	
	private String locknum;
	
	private String siteid;
	
	private String sitename;
	
	private String siteState;// 0 是异常；1.是正常
	
	private Date updateTime = new Date();

	public String getEmptynum() {
		return emptynum;
	}

	public void setEmptynum(String emptynum) {
		this.emptynum = emptynum;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLocknum() {
		return locknum;
	}

	public void setLocknum(String locknum) {
		this.locknum = locknum;
	}

	public String getSiteid() {
		return siteid;
	}

	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}

	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getLnglat() {
		return lnglat;
	}

	public void setLnglat(String lnglat) {
		this.lnglat = lnglat;
	}

	public String getUpdateTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(this.updateTime);
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
}
