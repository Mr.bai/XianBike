package com.baikeyang.bike.bean;

public class LatLng {
	private double lat;
	private double lng;

	public LatLng(double paramDouble1, double paramDouble2) {
		this.lat = paramDouble1;
		this.lng = paramDouble2;
	}

	public LatLng() {
	}

	public double getLat() {
		return this.lat;
	}

	public void setLat(double paramDouble) {
		this.lat = paramDouble;
	}

	public double getLng() {
		return this.lng;
	}

	public void setLng(double paramDouble) {
		this.lng = paramDouble;
	}
}
