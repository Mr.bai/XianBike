package com.baikeyang.bike.bean;

/**
 * 租赁记录
 * @author Administrator
 *
 */
public class BikeRecordBean {
	
	private String rentalTime;//租车时间
	
	private String rentalSite;//租车站点
	
	private String returnTime;//还车时间
	
	private String returnSite;//还车站点
	
	private String cost;//费用

	public String getRentalTime() {
		return rentalTime;
	}

	public void setRentalTime(String rentalTime) {
		this.rentalTime = rentalTime;
	}

	public String getRentalSite() {
		return rentalSite;
	}

	public void setRentalSite(String rentalSite) {
		this.rentalSite = rentalSite;
	}

	public String getReturnTime() {
		return returnTime;
	}

	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}

	public String getReturnSite() {
		return returnSite;
	}

	public void setReturnSite(String returnSite) {
		this.returnSite = returnSite;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}
	
}
