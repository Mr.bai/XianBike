package com.baikeyang.bike.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

public class ConvertLocalReuslt {
	
	private String status;
	
	private String info;// 高德
	private String message;// 腾讯
	
	private String infocode;
	
	private String locations;// 高德：String  腾讯：Array

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getInfocode() {
		return infocode;
	}

	public void setInfocode(String infocode) {
		this.infocode = infocode;
	}

	public String getLocations() {
		return locations;
	}

	public void setLocations(String locations) {
		this.locations = locations;//[{lng:12,lat:23}]
		try{
			List<String> list = new ArrayList<String>();
			list = JSONArray.parseArray(locations, String.class);
			if(null != list && list.size() > 0){
				this.locations = "";
				for(String str : list){
					LngLat lnglat = JSON.parseObject(str, LngLat.class);
					this.locations += lnglat.getLng()+","+lnglat.getLat()+";";
				}
			}
		}catch(Exception e){
		}
	}

}

class LngLat{
	private String lng;
	
	private String lat;

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}
}