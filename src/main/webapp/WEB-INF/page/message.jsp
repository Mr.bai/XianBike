<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%
	String ip = request.getHeader("x-forwarded-for");
	if (ip == null || ip.length() == 0
			|| "unknown".equalsIgnoreCase(ip)) {
		ip = request.getHeader("Proxy-Client-IP");
	}
	if (ip == null || ip.length() == 0
			|| "unknown".equalsIgnoreCase(ip)) {
		ip = request.getHeader("WL-Proxy-Client-IP");
	}
	if (ip == null || ip.length() == 0
			|| "unknown".equalsIgnoreCase(ip)) {
		ip = request.getRemoteAddr();
	}
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath %>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>移动终端访问</title>
</head>
<body>

<h1>请使用移动终端访问该页面，谢谢。</h1>
<div style="width: 100%;margin: 0 auto;text-align: center;font-weight: bold;color: red;">
<span>微信扫描 进入 西安市公共自行车 微信小程序↓↓↓↓↓（推荐）</span><br/>
<img alt="西安市公共自行车 微信小程序 入口扫描，扫我进入~" src="./resources/images/xazxc_pt.jpg" style="width: 280px;height: 310px;"><br/>
<span>==================================我是华丽的分割线====================================</span><br/>
<span>西安市公共自行车 网页版 入口扫描↓↓↓↓↓</span><br/>
<img alt="扫我，扫我，快扫我哦~" src="./resources/images/code.png"><br/>
</div>

说明：西安市公共自行车 微信小程序 和 网页APP 均为第三方开发应用。

<h5>提示：本App目前仅对 Windows Phone/Windows 10 Mobile 和 IOS 终端用户开放，谢谢你的关注。</h5>
<span>当前网络IP：</span><%=ip%><br/>
<span>当前使用系统：</span>${ua.platformType }&nbsp;${ua.platformSeries }<br/>
<span>当前使用浏览器：</span>${ua.browserType}&nbsp;${ua.browserVersion }
</body>
</html>