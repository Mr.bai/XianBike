<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>XA Bike Mobile Demo</title>
    <meta name="description" content="MSUI: Build mobile apps with simple HTML, CSS, and JS components.">
    <meta name="author" content="阿里巴巴国际UED前端">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">

    <!-- Google Web Fonts -->

    <link rel="stylesheet" href="./dist/css/sm.css">
    <link rel="stylesheet" href="./dist/css/sm-extend.css">
    <link rel="stylesheet" href="./assets/css/demos.css">

    <link rel="apple-touch-icon-precomposed" href="./assets/img/apple-touch-icon-114x114.png">
    <script src="./assets/js/zepto.js"></script>
    <script src="./assets/js/config.js"></script>

  </head>
  <body>

    <div class="page-group">
    <div class="page" id="page-index">
  <header class="bar bar-nav">
    <a class="icon icon-menu pull-left open-panel" data-panel=".panel-left"></a>
    <!-- <a class="button button-link button-nav pull-right open-popup" data-popup=".popup-about">
      <span class="icon icon-me"></span>
    </a> -->
    <h1 class="title">西安公共自行车APP</h1>
  </header>
  
  <!-- Search -->
  
  <div class="bar bar-header-secondary">
    <div class="searchbar row">
      <div class="search-input col-85 ">
        <label class="icon icon-search" for="search"></label>
        <input type="search" id='search' placeholder='输入关键字...'/>
      </div>
      <div class="col-15">
	      <a class="button button-fill button-primary my-btn-search" style="top: 0rem;padding: 0 0.1rem;"><span class="icon icon-search "></span></a>
      </div>
    </div>
  </div>

  <!-- Content -->
  <div class="content" id='page-index'>
	
	<div style="width: 100%; height: 100%;" id="myMap"></div>
    
  </div>
  
  
</div>



<div class="panel-overlay"></div>


<!-- 菜单区 （左侧）-->
<%@include file="left-meun.jsp" %>

<!-- （右侧） -->
<div class="panel panel-right panel-cover">
  <div class="content-block">
    <p>这是右侧栏，panel-cover模式</p>
    <p></p>
    <!-- Click on link with "close-panel" class will close panel -->
    <p><a href="#" class="close-panel">关闭</a></p>
  </div>
</div>


    </div>
    <script src="./dist/js/sm.js"></script>
    <script src="./dist/js/sm-extend.js"></script>
    <script src="./dist/js/sm-city-picker.js"></script>
    <script src="./assets/js/demos.js"></script>
    <script src="./ajax.js"></script>
     <script src="./myBike.js"></script>
     <script src="./XHRFactory.js"></script>
    <script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=9bcf01a96de88d6bda0a2a0e9f52d25e&plugin=AMap.Geocoder"></script> 
    <script type="text/javascript" src="./bikeMap.js"></script>
    <script type="text/javascript">
		var siteData  = new Array();
		
		$(function() {
		});
		
		$(document).on("click", ".my-btn-search", function() {
			searchBikeBtn($('#search').val());
		});
		
		function saveShowSetting(){
			$.alert('设置成功。');
			var showSettingVal = $("input[name='my-radio-showSetting']:checked").val();
			myBike.showSetting.state = showSettingVal;
			$.closeModal(".popup-showSetting")
			//initMarker();
		}
		
		function searchBikeBtn(val){
			var geocoder = new AMap.Geocoder({
	            city: "029", //城市，默认：“全国”
	            radius: 1000 //范围，默认：500
	        });
	        //地理编码,返回地理编码结果
	        geocoder.getLocation(val, function(status, result) {
	            if (status === 'complete' && result.info === 'OK') {
	                //console.log(result);
	                //console.log(result.geocodes[0].location);
	                clearMapAll();
	                var marker = new AMap.Marker({
	                    position: [result.geocodes[0].location.lng, result.geocodes[0].location.lat],
	                    draggable: false,
	                    cursor: 'move',
	                    icon: 'http://api0.map.bdimg.com/images/marker_red_sprite.png'
	                });
	                marker.setMap(map);
	                map.setCenter([result.geocodes[0].location.lng, result.geocodes[0].location.lat]);
	                map.setZoom(17);
	                searchBike(result.geocodes[0].location.lng, result.geocodes[0].location.lat)
	            }
	        });
		}
		
		function searchBike(valLng, valLat){
			$.ajax({
				 type:'post',
				 url:'getSearch.do',
				 data:{
					 lng:valLng,
					 lat:valLat,
					 radius:"2000"
				 },
				 dataType:'json',
				 success:function(data){
					 siteData = data;
					 if(null != myBike.markers.normalMarker && myBike.markers.normalMarker.length > 0){
						 clearMap(myBike.markers.normalMarker);
					 }
 					if(null != myBike.markers.exceptionMarker && myBike.markers.exceptionMarker.length > 0){
						 clearMap(myBike.markers.exceptionMarker);
					 }
					for(var i = 0; i < siteData.length; i++){
						 var siteInfo = siteData[i];
						 siteInfo.isNormal = 'exception';
						 if(siteInfo.siteState == 1){
							 siteInfo.isNormal = 'normal';
						 }
						 siteInfo.id = i;
						 //console.log(siteInfo);
						 addMarker(siteInfo);
					}
					map.setFitView();
				 }
			 });  
			
		}
		
	</script>
  </body>
</html>
