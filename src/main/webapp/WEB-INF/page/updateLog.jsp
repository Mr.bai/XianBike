<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="popup popup-updateLog">
  <header class="bar bar-nav">
    <!-- <a class="button button-link button-nav pull-right close-popup">
      关闭
    </a> -->
    <h1 class="title">关于西安公共自行车APP</h1>
  </header>
  <div class="content">
    <div class="content-inner">
      <div class="content-block" style="font-size: 10px;">
      	<h3>青年工作室 0.0.2.1 （2017年08月30日更新）</h3>
      	<p>
		  1、优化数据同步机制<br>
		  2、优化整体性能
		</p>
		<h3>青年工作室 0.0.2.0 （2017年08月13日更新）</h3>
		<p>
		  1、数据缓存本地，提高车辆查询相应速度<br>
		  2、支持Windows Mobile 10设备的访问
		</p>
      	<h3>青年工作室 0.0.1.180 （2017年03月12日更新）</h3>
		<p>
		  1、动态实时显示当前位置，更加方便的查看周边车位情况<br>
		  2、扩大当前位置车位的搜索范围<br>
		  3、新增 显示个人当前位置
		</p>
      	<h3>青年工作室 0.0.1.180 （2016年08月14日更新）</h3>
		<p>
		  1、添加设备访问APP过滤机制<br>
		</p>
		<h3>青年工作室 0.0.1.177 （2016年07月28日更新）</h3>
		<p>
		  1、支持地理位置搜索车辆信息<br/>
		  2、调整页面初始车辆信息查询策略<br/>
		  3、优化系统查询性能<br/>
		</p>
        <h3>青年工作室 0.0.1.156 （2016年06月14日更新）</h3>
		<p>
		  1、支持不同状态的站点车辆信息查看 <br/>
		  2、取消谷歌地图层<br/>
		  3、对站点的经纬度纠偏，提高站点再地图的位置精确度<br/>
		</p>
        <p></p>
        <p><a class="button close-popup">确定</a></p>
      </div>
    </div>
  </div>
</div>
