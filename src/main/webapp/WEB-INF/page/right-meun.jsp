<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="panel panel-left panel-reveal">

	<div class="content-block" style="text-align: center;"><span class="icon icon-settings"  style="font-size: 1.85em;color: #fff;"></span></div>
    <div class="list-block ">
      <ul>
        <!-- <li><a href="#" class="item-link list-button">个人中心</a></li>
        <li><a href="#" class="item-link list-button">帮助</a></li> -->
        <li><a href="#" class="item-link list-button open-popup" data-popup=".popup-showSetting">显示设置</a></li>
        <li><a href="#" class="item-link list-button open-popup" data-popup=".popup-about">关于我们</a></li>
        <li><a href="#" class="item-link list-button close-panel">返回</a></li>
      </ul>
    </div>
</div>


<!-- 显示设置 -->
<div class="popup popup-showSetting">
  <header class="bar bar-nav">
    <!-- <a class="button button-link button-nav pull-right close-popup">
      关闭
    </a> -->
    <h1 class="title">显示设置</h1>
  </header>
  <div class="content">
    <div class="content-inner">
      <div class="content-block">
        
        	<div class="list-block media-list">
		      <ul>
		        <li>
		          <label class="label-checkbox item-content">
		            <input type="radio" name="my-radio-showSetting" checked="checked" value="1">
		            <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
		            <div class="item-inner">
		              <div class="item-title-row">
		                <div class="item-title">正常网点(默认)</div>
		              </div>
		            </div>
		          </label>
		        </li>
		        <li>
		          <label class="label-checkbox item-content">
		            <input type="radio" name="my-radio-showSetting" value="2">
		            <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
		            <div class="item-inner">
		              <div class="item-title-row">
		                <div class="item-title">异常网点</div>
		              </div>
		            </div>
		          </label>
		        </li>
		        <li>
		          <label class="label-checkbox item-content">
		            <input type="radio" name="my-radio-showSetting" value="3">
		            <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
		            <div class="item-inner">
		              <div class="item-title-row">
		                <div class="item-title">全部网点</div>
		              </div>
		            </div>
		          </label>
		        </li>
		      </ul>
		    </div>
        <p></p>
        <p><a class="button" onclick="saveShowSetting();">确定</a></p>
        <p><a class="button close-popup" id="showSetting_back">返回</a></p>
      </div>
    </div>
  </div>
</div>
