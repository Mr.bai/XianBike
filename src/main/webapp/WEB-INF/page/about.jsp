<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="popup popup-about">
  <header class="bar bar-nav">
    <!-- <a class="button button-link button-nav pull-right close-popup">
      关闭
    </a> -->
    <h1 class="title">关于西安公共自行车APP</h1>
  </header>
  <div class="content">
    <div class="content-inner">
      <div class="content-block">
        <!-- <p>
        	为深入推进缓堵保畅工作，进一步缓解和改善城市交通拥堵的状况，不断提升交通通达性和群众满意度，按照市委、市政府的总体部署，
        	建设城市公共自行车服务系统，加快实施公共自行车站点和专用道建设，重点解决地铁、公交站点与居民区之间的短途接驳问题，改善
        	市民交通出行结构，倡导绿色出行。
        </p> -->
        <p>
        	2013年3月26日，西安城市公共自行车服务管理有限责任公司正式成立，隶属于西安市公共交通总公司，注册资金1500万元，下属5部1室
        	及10个特级站，具体负责西安市公共自行车系统及站点的筹备建设和运营工作。2015年末，上架自行车将达到42000辆，服务站点1460个
        	，服务区域550.5平方公里，日均使用量将超过216000人次。
		</p>
        <p></p>
        <p><a class="button close-popup">确定</a></p>
      </div>
    </div>
  </div>
</div>
